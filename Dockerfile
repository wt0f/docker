FROM docker:stable

RUN apk update && apk add jq curl && \
    mkdir -p /root/.docker/cli-plugins && \
    BUILDX_URL=$(curl https://api.github.com/repos/docker/buildx/releases/latest | jq -r '.assets[].browser_download_url' | grep linux-amd64) && \
    wget $BUILDX_URL -O ~/.docker/cli-plugins/docker-buildx && \
    chmod +x ~/.docker/cli-plugins/docker-buildx
